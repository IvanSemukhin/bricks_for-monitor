/*
класс связанный с графикой QML
используется     QQuickWidget и QQmlContext
этот класс "внутренних" данных на С++
без использования в QML JavaScript.

*/
#ifndef SPEEDDATA_H
#define SPEEDDATA_H

#include <QtWidgets>
#include <QTimer>
#include <QDebug>
#include <QMouseEvent>
#include <QQuickWidget>
#include <QQmlContext>
#include <random>
#include <QDialog>

class Speeddata : public QWidget{
Q_OBJECT
private:
    Q_PROPERTY(qint32 speed WRITE setSpeedValue READ speedValue NOTIFY speedValueChanged)
    Q_PROPERTY(qint32 min WRITE setMinValue READ minValue NOTIFY minValueChanged)
    Q_PROPERTY(qint32 max WRITE setMaxValue READ maxValue NOTIFY maxValueChanged)
    qint32 speed;
    qint32 min;
    qint32 max;
    QTimer* ch_speed_timer;
public:
    explicit Speeddata(QWidget *parent = nullptr);

    void setSpeedValue(const qint32& spd);
    qint32 speedValue()const;
    void setMinValue(const qint32& spd);
    qint32 minValue()const;
    void setMaxValue(const qint32& spd);
    qint32 maxValue()const;

protected:
    void mouseDoubleClickEvent(QMouseEvent *event);
signals:
    void speedValueChanged(qint32);
    void minValueChanged(qint32);
    void maxValueChanged(qint32);

private slots:
    void slotUpdateSpeedValue();
    void slotStart();
    void slotStop();
    void slotSetSpeedValue(const qint32& val);
};

#endif // SPEEDDATA_H
