#include "receiver.h"

Receiver::Receiver(QTcpSocket* parent /*= nullptr*/) : QTcpSocket(parent),
    m_nextBlockSize(0)
{
}

void Receiver::slotReadData(){  // слт приёма данных из Eth Net
    // выполнить здесь обработчик приёма по условленному формату приёма/передачи.
    QDataStream in(this);
    in.setVersion(QDataStream::Qt_5_9);
    for(;;){
        if(!m_nextBlockSize){
            if(this->bytesAvailable() < sizeof(quint16)){
                break;
            }
            in >> m_nextBlockSize;
        }
        if(this->bytesAvailable() < m_nextBlockSize){
            break;
        }
        qint32 value;
        in >> value;
        qDebug() << "receive from gen: " << value;
        m_nextBlockSize = 0;
        emit newValue(value);
    }
}

void Receiver::start(){
    this->connectToHost(QHostAddress::LocalHost, PORT);
    if(!this->waitForConnected(3000)){      // нет коннекта с сервером выходит из приложения.
        qDebug() << this->errorString();
        exit(-1);
    }
    if(this->state() == QAbstractSocket::ConnectedState){
        QObject::connect(this, SIGNAL(readyRead()), this, SLOT(slotReadData()));
        qDebug() << "Connect succsess!";
    }
}
