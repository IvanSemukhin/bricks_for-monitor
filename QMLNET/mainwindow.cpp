#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    Speeddata* spd = new Speeddata(this);
    Receiver* receiver = new Receiver;
    QObject::connect(receiver, SIGNAL(newValue(qint32)), spd, SLOT(slotSetSpeedValue(const qint32&)));
    // receiver принимает данные из сети, а слот обновляет данные "внутреннего" класса, который связан с QML
    QWidget* central = new QWidget(this);
    QVBoxLayout* panel = new QVBoxLayout(central);

    panel->addWidget(spd);
    central->setLayout(panel);
    setCentralWidget(central);
    receiver->start();
}

MainWindow::~MainWindow()
{

}
