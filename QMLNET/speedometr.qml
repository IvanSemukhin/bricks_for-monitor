import QtQuick 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4

Rectangle{
    id: rama_speed
    width: 300
    height: width
    color: "#494d53"

    CircularGauge{
        id: cif
        value: mySpeed.speed
        anchors.centerIn: parent
        anchors.fill: parent
        anchors.margins: 25
        minimumValue: mySpeed.min
        maximumValue: mySpeed.max
        Behavior on value {
            NumberAnimation{
                duration: 500
            }
        }
        style: CircularGaugeStyle{
            tickmarkStepSize: 10
        }
        Text {
            id: valueText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            text: cif.value.toFixed(0) + " km/h"
            color: cif.value < -25 || cif.value > 25 ? "red" : "blue"
        }
    }
}
