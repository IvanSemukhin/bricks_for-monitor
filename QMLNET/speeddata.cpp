#include "speeddata.h"

Speeddata::Speeddata(QWidget *parent /*= nullptr*/):
    QWidget(parent),
    speed(0),
    min(0),
    max(50)
{
    QQuickWidget* pv = new QQuickWidget(QUrl("qrc:///speedometr.qml"));
    QQmlContext* pcon = pv->rootContext();
    pcon->setContextProperty("mySpeed", this);  // в исходно коде QML можно обращаться к property класса C++
    pv->setMaximumSize(300, 300);
    QVBoxLayout* box = new QVBoxLayout;
    box->setMargin(0);
    box->addWidget(pv);
    setLayout(box);
    setMaximumSize(pv->size());
    ch_speed_timer = new QTimer;
    connect(ch_speed_timer, SIGNAL(timeout()), this, SLOT(slotUpdateSpeedValue()));
}
//-----------------------------------------------------------------------------
void Speeddata::setSpeedValue(const qint32& spd){
    speed = spd;    // поле данных связанное с QML (property and method's)
    emit speedValueChanged(speed);
}
//-----------------------------------------------------------------------------
qint32 Speeddata::speedValue()const{
    return speed;
}
//-----------------------------------------------------------------------------
void Speeddata::setMinValue(const qint32& min){
    this->min = min;
    emit minValueChanged(this->min);
}
//-----------------------------------------------------------------------------
qint32 Speeddata::minValue()const{
    return min;
}
//-----------------------------------------------------------------------------
void Speeddata::setMaxValue(const qint32& max){
    this->max = max;
    emit maxValueChanged(this->max);
}
//-----------------------------------------------------------------------------
qint32 Speeddata::maxValue()const{
    return max;
}
//-----------------------------------------------------------------------------
void Speeddata::slotUpdateSpeedValue()
{
    static std::mt19937 gen;
    std::uniform_int_distribution<qint32> uid(this->min, this->max);
    qint32 tmp_val = uid(gen);
    this->setSpeedValue(tmp_val);
//    qDebug() << "Speeddata::slotUpdateSpeedValue: " << tmp_val;
//    speed == max ? speed = min : speed = max;
//    this->setSpeedValue(speed);
}
//-----------------------------------------------------------------------------
void Speeddata::slotStart()
{
    ch_speed_timer->start(700);
}
//-----------------------------------------------------------------------------
void Speeddata::slotStop()
{
    ch_speed_timer->stop();
}
void Speeddata::slotSetSpeedValue(const qint32 &val){   // слот для изменения значения "внутренних" с++.
    setSpeedValue(val); // установить значение. т.к. значение связано с графикой QML произойдёт отрисовка.
}

void Speeddata::mouseDoubleClickEvent(QMouseEvent *event){  // обработчик widget для изменения свойств
    Q_UNUSED(event);
//    qDebug() << "mouseDoubleClickEvent(): "<< event->x() << event->y();
    QDialog* dial = new QDialog;

    QSpinBox* max_val = new QSpinBox;
    QSpinBox* min_val = new QSpinBox;
    min_val->setMinimum(-100);
    max_val->setMaximum(300);
    min_val->setValue(this->minValue());
    max_val->setValue(this->maxValue());


    QLabel* min_lbl = new QLabel("MIN");
    QLabel* max_lbl = new QLabel("MAX");

    QPushButton* cmdOk = new QPushButton("&OK");
    QObject::connect(cmdOk, SIGNAL(clicked()), dial, SLOT(accept()));

    QGridLayout* panel_box = new QGridLayout();
    panel_box->addWidget(min_lbl, 0, 0);
    panel_box->addWidget(min_val, 0, 1);
    panel_box->addWidget(max_lbl, 1, 0);
    panel_box->addWidget(max_val, 1, 1);
    panel_box->addWidget(cmdOk, 2, 2);
    dial->setLayout(panel_box);

    if(dial->exec() == QDialog::Accepted){
        this->setMinValue(min_val->value());
        this->setMaxValue(max_val->value());
    }
    delete panel_box;
    delete dial;
}
