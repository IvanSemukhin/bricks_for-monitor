/*
класс приёма данных из Eth NET

*/
#ifndef RECEIVER_H
#define RECEIVER_H

#include <QTcpSocket>
#include <QTcpServer> // for QHostAddress
#include <QByteArray>
#include <QDataStream>

const quint16 PORT = 32500;

class Receiver : public QTcpSocket
{
Q_OBJECT
public:
    explicit Receiver(QTcpSocket* parent = nullptr);
    void start();
private:
    quint16 m_nextBlockSize;
private slots:
    void slotReadData();
signals:
    void newValue(qint32);
};

#endif // RECEIVER_H
