#include "generator.h"

Generator::Generator(QTcpServer* parent /*= nullptr*/) : QTcpServer(parent)
{
    this->setMaxPendingConnections(1);  //  только 1 клиент
}
//-----------------------------------------------------------------------------
void Generator::startGen()
{
    this->listen(QHostAddress::LocalHost, PORT);
    if(!this->isListening()){
        qDebug() << this->errorString();
        return;
    }
    qDebug() << QString::number(this->serverPort()) << " Linsten...";
    connect(this, SIGNAL(newConnection()), this, SLOT(slotAcceptConnection())); // вызвать слод обработки подключения клиента
    this->waitForNewConnection(-1);
}
//-----------------------------------------------------------------------------
void Generator::slotAcceptConnection()
{
    qDebug() << "CALL: slotAcceptConnection()";
    m_gen_socket = this->nextPendingConnection();   // принять ожидающего клиента
    this->close();                                  // вывести сервер из состояния прослушки(только 1 клиент)
    QObject::connect(m_gen_socket, SIGNAL(error(QAbstractSocket::SocketError)),
                     this, SLOT(slotSocketError(QAbstractSocket::SocketError)));
    if(m_gen_socket->state() == QAbstractSocket::ConnectedState){
        QTimer* tim = new QTimer(this);
        connect(tim, SIGNAL(timeout()), this, SLOT(slotWriteData()));   // вызвать слот отправки данных клиенту
        tim->start(1000);
    }
    else{
        qDebug() <<"m_gen_socket not CONNECTED!!!";
    }
    qDebug() << "slotAcceptConnection() END";
}
//-----------------------------------------------------------------------------
void Generator::slotWriteData()
{
    // здесь задать отправку данных по согласованному формату
    qint32 value = qrand()%100;
    qDebug() << "Send to Client " << value;
    QDataStream out(&m_gen_data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_9);

    out << quint16(0) << value;     // резерв под размер пакета данных на нулевой позиции и сами данные
    out.device()->seek(0);
    out << quint16(m_gen_data.size() - sizeof(quint16));    // записать фактический размер данных
    m_gen_socket->write(m_gen_data);                        // запись данных в Eth NET
}
//-----------------------------------------------------------------------------
void Generator::slotSocketError(QAbstractSocket::SocketError socketError){
    qDebug() << socketError;
}
