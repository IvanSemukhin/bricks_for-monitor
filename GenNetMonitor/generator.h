/*
Генератор данных для отправки в Eth NET.
Идея насчёт шифрования данных в канале:
    возможно не придётся шифровать.
    пусть сервер ждёт только одного единственного подключения.
    после подключения сервер перестаёт слушать сеть.
*/
#ifndef GENERATOR_H
#define GENERATOR_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>
#include <QDataStream>

const quint16 PORT = 32500;

class Generator : public QTcpServer
{
Q_OBJECT
public:
    explicit Generator(QTcpServer* parent = nullptr);
    void startGen();
private:
    QByteArray m_gen_data;
    QTcpSocket* m_gen_socket;
private slots:
    void slotAcceptConnection();
    void slotWriteData();
    void slotSocketError(QAbstractSocket::SocketError socketError);
};

#endif // GENERATOR_H
