//#include "deviceproperty.h"
#include "xmlparser.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication* app = new QGuiApplication(argc, argv);

    //qmlRegisterType<DeviceProperty>("DevProp", 1, 0, "DeviceProperty");
    XMLParser parser("C:/Users/SemuhinIV/Documents/QMLTest/TG16M.xml"); // fix!!! path
    parser.load_config();

    QQmlApplicationEngine* engine = new QQmlApplicationEngine;
    QQmlContext *ctxt = engine->rootContext();
    ctxt->setContextProperty("myModel", QVariant::fromValue(parser.get_dataList()));

    engine->load(QUrl(QStringLiteral("qrc:/main.qml")));
    if(engine->rootObjects().isEmpty())
        return -1;

    return app->exec();
}
