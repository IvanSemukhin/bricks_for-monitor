#include "deviceproperty.h"

DeviceProperty::DeviceProperty(QObject* parent):
    QObject(parent)
{
}

DeviceProperty::DeviceProperty(QString name, quint8 adr, quint8 com, QObject* parent):
    QObject(parent),
    m_name(name),
    m_adr(adr),
    m_com(com)
{
}

QString DeviceProperty::name()const{return m_name;}
quint8 DeviceProperty::adr()const{return m_adr;}
quint8 DeviceProperty::com()const{return m_com;}

void DeviceProperty::setName(const QString& name)
{
    if(m_name != name){
        m_name = name;
        emit nameChanged();
    }
}

void DeviceProperty::setAdr(const quint8& adr)
{
    if(m_adr != adr){
        m_adr = adr;
        emit adrChanged();
    }
}

void DeviceProperty::setCom(const quint8& com)
{
    if(m_com != com){
        m_com = com;
        emit comChanged();
    }
}
