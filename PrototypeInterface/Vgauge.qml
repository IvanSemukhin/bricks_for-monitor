import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4

Item {
    width: 100; height: 240
    Rectangle{
        anchors.fill: parent
        color: "#494d53"
        radius: 5
        Text{
            width: parent.width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 14
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Давление в двигателе")
            wrapMode: Text.Wrap
        }
        Gauge{
            id: device
            y: 40
            value: maximumValue / 2
            SequentialAnimation on value{
                loops: Animation.Infinite
                PropertyAnimation {to: 83; duration: 500}
                PropertyAnimation {to: 80; duration: 500}
            }
        }
        Text{
            property string measure: " MPA"
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            text: Math.round(device.value, 0) + measure
            font.pixelSize: 20
            wrapMode: Text.Wrap
        }
    }
}
