import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4

Item{
    width: 200; height: 100
    property alias nameValue: valText.text
    Rectangle{
        anchors.fill: parent
        color: "#494d53"
        radius: 5
        ColumnLayout{
            anchors.fill: parent
            Text {
                id: valText
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Давление в двигателе")
                font.pixelSize: 16
            }
            Gauge{
                id: device
                anchors.left: parent.left
                anchors.right: parent.right
                orientation: Qt.Horizontal
                tickmarkAlignment: Qt.AlignTop
                value: maximumValue / 2
                SequentialAnimation on value{
                    loops: Animation.Infinite
                    PropertyAnimation {to: 83; duration: 500}
                    PropertyAnimation {to: 80; duration: 500}
                }
            }
            Text{
                property string measure: " MPA"
                anchors.horizontalCenter: parent.horizontalCenter
                text: Math.round(device.value, 0) + measure
                font.pixelSize: 20
            }
        }
    }
}
