import QtQuick 2.10
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4

Rectangle{
    id: rama_speed
    width: 150
    height: width
    color: "#494d53"
    radius: 5

    CircularGauge{
        id: cif
        anchors.centerIn: parent
        anchors.fill: parent
        anchors.margins: 5
        maximumValue: 120
        value: maximumValue / 2
        SequentialAnimation on value{
            loops: Animation.Infinite
            PropertyAnimation {to: 85; duration: 5000}
            PropertyAnimation {to: 75; duration: 5000}
        }
        style: CircularGaugeStyle{
            tickmarkStepSize: 10
        }
        Text {
            id: valueText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            text: cif.value.toFixed(0) + " km/h"
            color: cif.value < -25 || cif.value > 80 ? "orange" : "lightgreen"
        }
    }
    DialBlk{id: dialblk}
    MouseArea{
        anchors.fill: parent
        onDoubleClicked: {
            console.log("Open Dialog with model view");
            dialblk.open();
        }
    }
}
