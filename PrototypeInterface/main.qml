import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

ApplicationWindow{
    id: mainWindow
    title: qsTr("PROTOTYPE")
    visible: true
    minimumWidth: 640
    minimumHeight: 640
    maximumWidth: 640
    maximumHeight: 640


    footer: TabBar{
        id:mainBar
        width: parent.width
        TabButton{
            text: qsTr("&Dashboard")
        }
        TabButton{
            text: qsTr("&System setup")
        }
        TabButton {
            text: qsTr("&Logging")
        }
    }

    StackLayout {
        anchors.fill: parent
        currentIndex: mainBar.currentIndex
        Dashboard{}
        Systemsetup{}
        Logging{}
    }
}
