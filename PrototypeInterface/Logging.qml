import QtQuick 2.10
import QtQuick.Controls 2.3

Page{
    Rectangle{
        anchors.fill: parent
        color: "lightgreen"
        Button{
            anchors.fill: parent
            onClicked: {
                console.log(mainWindow.height, mainWindow.width)
            }
            Text {
                anchors.centerIn: parent
                text: qsTr("Logging")
            }
        }
    }
}
