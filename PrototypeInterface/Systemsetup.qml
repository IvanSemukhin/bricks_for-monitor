import QtQuick 2.10
import QtQuick.Controls 2.3

Page{
    Rectangle{
        anchors.fill: parent
        color: "steelblue"
        Text {
            anchors.centerIn: parent
            text: qsTr("System setup")
        }
    }
}
