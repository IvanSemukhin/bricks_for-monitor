import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4

Page{
    background: Rectangle{
        anchors.fill: parent
        color: "#494949"
    }
    ColumnLayout{
        id: dashboardMainLayout
        anchors.fill: parent

        GroupBox{
            id: rowBox
            Layout.fillHeight: true
            Layout.fillWidth: true

            RowLayout{
                anchors.fill: parent
                GridLayout{
                    id: speedLayout
                    Layout.fillWidth: true
                    rows: 2
                    columns: 2
                    columnSpacing: 1
                    rowSpacing: 1
                    Mcircular{}
                    Mcircular{}
                    Mcircular{}
                    Mcircular{}
                }
                GridLayout{
                    id: imageLayout
                    property int size: 75
                    Layout.fillWidth: true
                    rows: 4
                    columns: 4
                    columnSpacing: 1
                    rowSpacing: 1
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                    Rectangle{color: "green"; width: parent.size; height: parent.size}
                }
            }
        }
        GroupBox{
            id: gaugeLayout
            Layout.fillHeight: true
            Layout.fillWidth: true

            RowLayout{
                Vgauge{}
                Vgauge{}
                Vgauge{}
                Vgauge{}
                ColumnLayout{
                    Hgauge{}
                    Hgauge{}
                }
            }
        }
    }
}
