#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <QList>
#include <QDomNode>
#include <memory>

class DeviceProperty;

class XMLParser
{
public:
    XMLParser(QString path_to_config);
    QString get_path()const;
    void load_config();
    void traverse(const QDomNode& node);
    void debug_show();
    QList<QObject*> get_dataList()const;
    //QList < std::shared_ptr <QObject> > get_dataList()const;

private:
    QString m_path_to_config;
    QList<QObject*> m_lst_dev_prop;
    //QList < std::shared_ptr <QObject> > m_lst_dev_prop;
};

#endif // XMLPARSER_H
