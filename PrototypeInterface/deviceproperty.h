#ifndef DEVICEPROPERTY_H
#define DEVICEPROPERTY_H

#include <QObject>

class DeviceProperty : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(quint8 adr READ adr WRITE setAdr NOTIFY adrChanged)
    Q_PROPERTY(quint8 com READ com WRITE setCom NOTIFY comChanged)

public:
    DeviceProperty(QObject* parent = nullptr);
    DeviceProperty(QString name = "Unknown name",
                            quint8 adr = 0x00,
                            quint8 com = 0x00,
                            QObject* parent = nullptr);

    QString name()const;
    void setName(const QString& name);

    quint8 adr()const;
    void setAdr(const quint8& adr);

    quint8 com()const;
    void setCom(const quint8& com);

private:
    QString m_name;
    quint8 m_adr;
    quint8 m_com;

signals:
    void nameChanged();
    void adrChanged();
    void comChanged();
};

#endif // DEVICEPROPERTY_H
