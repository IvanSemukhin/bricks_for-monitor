#include "xmlparser.h"
#include "deviceproperty.h"
#include <QtXml>
#include <QDebug>

XMLParser::XMLParser(QString path_to_config):
    m_path_to_config(path_to_config)
{
}

QString XMLParser::get_path()const
{
    return m_path_to_config;
}

void XMLParser::load_config()
{
    QFile file(m_path_to_config);
    if(file.open(QIODevice::ReadOnly)){
        QDomDocument document;
        if(document.setContent(&file)){
            QDomElement domElement = document.documentElement();
            traverse(domElement);
        }
        else{qDebug() << "ERROR! Broken XML file: " << m_path_to_config;}
        file.close();   // no in else block!
    }
    else{qDebug() << "ERROR! Canot open file: "<< m_path_to_config;}
}

void XMLParser::traverse(const QDomNode &node){
    QDomNode domNode = node.firstChild();
    while(!domNode.isNull()){
        if(domNode.isElement()){
            QDomElement domElement = domNode.toElement();
            if(!domElement.isNull()){
                if(domElement.tagName() == "block"){
                    QString name = domElement.attribute("name", "");
                    quint8 adr = domElement.attribute("address", "").toUInt(nullptr, 16);
                    quint8 com = domElement.attribute("command", "").toUInt(nullptr, 16);
                    DeviceProperty* dp = new DeviceProperty(name, adr, com);
                    //std::shared_ptr<DeviceProperty> dp(new DeviceProperty{name, adr, com});
                    m_lst_dev_prop.append(dp);
                }
            }
        }
        traverse(domNode);
        domNode = domNode.nextSibling();
    }
}

void XMLParser::debug_show()
{
    qDebug() << m_lst_dev_prop;
}

QList<QObject*> XMLParser::get_dataList()const
//QList< std::shared_ptr <QObject> > XMLParser::get_dataList()const
{
    return m_lst_dev_prop;
}
