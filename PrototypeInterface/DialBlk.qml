import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.3

Dialog {
    title: "Select Block"
    width: 360; height: 360
    contentItem: ListView{
        id: view
        model: myModel
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10
        clip: true

        delegate: Item{
            id: listDelegate

            property var view: ListView.view
            property var isCurrent: ListView.isCurrentItem

            width: view.width
            height: 40
            Rectangle{
                anchors.fill: parent
                radius: height / 2
                anchors.margins: 5
                color: "red"
            }
            Text{
                anchors.centerIn: parent
                renderType: Text.NativeRendering
                //text: "%1%2".arg(model.text).arg(isCurrent ? " *" : "") // взять из каждого элемента модели, по условию добавить строку
                text: model.modelData.name
            }
        }
    }
}
